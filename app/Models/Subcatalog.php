<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcatalog extends Model
{
	use SoftDeletes;
    protected $table = 'subcatalogs';
    protected $primaryKey = 'scat_id';

}
