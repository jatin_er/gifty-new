<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Catalog;
use App\Models\Subcatalog;
use App\Models\Brand;
use App\Models\Attribute;
use App\Models\Tax;
use App\Models\Zone;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Weight;
use App\Models\ShippingServices;
use App\Models\Product;
use Session;
use DB;

class VendorController extends Controller
{
    public function catalog()
    {
        if(session()->has('users_id'))
        {
            $data['title'] = "Catalog";
            return view('vendor.catalog',compact('data'));
        }
        else
        {
            return redirect()->intended('/login');
        }
    }
    public function add_catalog(Request $request)
    {   
		if($request->hasFile('image'))
		{
			$image=$request->file('image');
			$extension=strtolower($request->image->getClientOriginalExtension());
			if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
			{
				$po_logoname = time().'.'.$request->file('image')->getClientOriginalExtension();
				$dir = 'public/product/catalog';
				$request->file('image')->move($dir, $po_logoname);
			}
			else
			{
				return ['status' => 'chkimg', 'redirect' => '', 'msg' =>'Please check image(Only png, jpg, jpeg allowed)'];
				$po_logoname = "";
			}
		}
		else
		{
			return ['status' => 'chkimg', 'redirect' => '', 'msg' =>'Please check image(Only png, jpg, jpeg allowed)bgh'];
			$po_logoname = "";
		}
		if($po_logoname!='')
		{
	    	$category=ucfirst($request->get('category'));
	    	date_default_timezone_set('Asia/Kolkata');
			$create_date=date("Y-m-d");
			$create_time=date("h:i:sa");
			$checkcategory=Catalog::where('cat_name',$category)->first();
			if($checkcategory)
			{
				return ['status' => 'exist', 'redirect' => '', 'msg' =>'Category already exist'];
			}
			else
			{
				$newsl=strtolower($request->get('category'));
				$slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $newsl);
				$checkcategorynew=Catalog::where('cat_slug',$slug)->first();
				if($checkcategorynew)
				{
					$newrand=rand(1,9);
					$slug=$slug.$newrand;
				}
				$maxValue = Catalog::max('cat_order');
				$neworder=$maxValue + 1;
				$maindataarr=array('cat_name' => $category,
									'cat_image'=>$po_logoname,
									'cat_slug'=>$slug,
									'seokey'=>$request->get('seokey'),
									'meta_description'=>$request->get('meta_description'),
									'cat_order'=>$neworder,
									'cat_status'=>'1',
									'cat_date'=>$create_date,
									'cat_time'=>$create_time,
								 );
				$datasave=Catalog::insert($maindataarr);
				if($datasave)
				{
					return ['status' => 'success', 'redirect' => '', 'msg' =>'Category Added Successfully'];
				}
				else
				{
					return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
				}
			}
		}
    }
    public function sub_catalog()
    {
        if(session()->has('users_id'))
        {
            $data['title'] = "Sub Catalog";
            $productdata=Catalog::where('cat_status','1')->orderBy('cat_name','ASC')->get();
            return view('vendor.sub-catalog',compact('data','productdata'));
        }
        else
        {
            return redirect()->intended('/login');
        }
    }
    public function add_subcatalog(Request $request)
    {   
    	$subcategory=ucfirst($request->get('subcategory'));
    	$category=$request->get('category');
    	$seokey=$request->get('seokey');
    	$meta_description=$request->get('meta_description');
    	date_default_timezone_set('Asia/Kolkata');
		$create_date=date("Y-m-d");
		$create_time=date("h:i:sa");
		$maxValue = Subcatalog::max('scat_order');
		$neworder=$maxValue + 1;
		 $slugval=strtolower($request->get('subcategory'));
		 $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $slugval);
		 $checkcategory=Subcatalog::where('scat_name',$subcategory)->where('cat_id',$category)->first();
		if($checkcategory)
		{
			return ['status' => 'exist', 'redirect' => '', 'msg' =>'Subcategory already exist'];
		}
		else
		{
			$checkcategorynew=Subcatalog::where('scat_slug',$slug)->first();
			if($checkcategorynew)
			{
				$newrand=rand(1,9);
				$slug=$slug.$newrand;
			}
			$maindataarr=array('cat_id' => $category,
							'scat_name'=>$subcategory,
							'scat_slug'=>$slug,
							'seokey'=>$request->get('seokey'),
							'meta_description'=>$request->get('meta_description'),
							'scat_order'=>$neworder,
							'scat_status'=>'1',
							'scat_date'=>$create_date,
							'scat_time'=>$create_time,
						 );
			$datasave=Subcatalog::insert($maindataarr);
			if($datasave)
			{
				return ['status' => 'success', 'redirect' => '', 'msg' =>'Subcategory Added Successfully'];
			}
			else
			{
				return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
			}
		}
    }
    public function tax()
    {
        if(session()->has('users_id'))
        {
        	$zone=zone::where('zone_status','1')->get();
            $data['title'] = "Tax";
            return view('vendor.taxes',compact('data','zone'));
        }
        else
        {
            return redirect()->intended('/login');
        }
    }
    public function add_tax(Request $request)
    {   
    	$tax_name=ucfirst($request->get('$tax_name'));
    	$tax_rate=$request->get('$tax_rate');
    	$based_on=$request->get('based_on');
    	if(!empty($request->get('tax_zone')))
		{
			$tax_zone=implode(',',$request->get('tax_zone'));
		}
		else
		{
			$tax_zone="";
		}
    	$tax_rule=$request->get('tax_rule');
    	date_default_timezone_set('Asia/Kolkata');
		$create_date=date("Y-m-d");
		$create_time=date("h:i:sa");
		$checktax=Tax::where('tax_name',$tax_name)->first();
		if($checktax)
		{
			return ['status' => 'exist', 'redirect' => '', 'msg' =>'Tax already exist'];
		}
		else
		{
			$maxValue = Tax::max('tax_order');
			$neworder=$maxValue + 1;
			$maindataarr=array('tax_name' => $tax_name,
								'tax_rate'=>$tax_rate,
								'tax_zone'=>$tax_zone,
								'based_on'=>$based_on,
								'tax_rule'=>$tax_rule,
								'tax_order'=>$neworder,
								'tax_status'=>'1',
								'tax_date'=>$create_date,
								'tax_time'=>$create_time,
							 );
			$datasave=Tax::insert($maindataarr);
			if($datasave)
			{
				return ['status' => 'success', 'redirect' => '', 'msg' =>'Tax Added Successfully'];
			}
			else
			{
				return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
			}
		}
    }
    public function zone()
    {
        if(session()->has('users_id'))
        {
        	$country=Country::all();
            $data['title'] = "Zone";
            return view('vendor.zone',compact('data','country'));
        }
        else
        {
            return redirect()->intended('/login');
        }
    }
    public function search_state(Request $request)
    {
        $country_id=$request->get('country_id');
        $state_data=State::where('country_id',$country_id)->get();
        $state='<option value="">Select State</option>';
        foreach($state_data as $stdata)
        {
            $state.='<option value="'.$stdata->id.'">'.$stdata->name.'</option>';
        }
        echo $state;
    }
    public function search_city(Request $request)
    {
        $state_name=$request->get('state_name');
        $city_data=City::where('state_id',$state_name)->get();
        $city='<option value="">Select City</option>';
        foreach($city_data as $ctdata)
        {
            $city.='<option value="'.$ctdata->id.'">'.$ctdata->name.'</option>';
        }
        echo $city;
    }
    public function add_zone(Request $request)
    {   
    	$country_name=$request->get('country_name');
        $state_name=$request->get('state_name');
        $city_name=$request->get('city_name');
        $zone_name=$request->get('zone_name');
        $zone_code=$request->get('zone_code');
    	date_default_timezone_set('Asia/Kolkata');
		$create_date=date("Y-m-d");
		$create_time=date("h:i:sa");
		$checktax=Zone::where('zone_country',$country_name)->where('zone_state',$state_name)->where('zone_city',$city_name)->first();
		if($checktax)
		{
			return ['status' => 'exist', 'redirect' => '', 'msg' =>'Zone already exist'];
		}
		else
		{
			$maxValue = Zone::max('zone_order');
			$neworder=$maxValue + 1;
			$maindataarr=array('zone_country' => $country_name,
								'zone_state'=>$state_name,
								'zone_city'=>$city_name,
								'zone_name'=>$request->get('zone_name'),
								'zone_code'=>$request->get('zone_code'),
								'zone_order'=>$neworder,
								'zone_status'=>'1',
								'zone_date'=>$create_date,
								'zone_time'=>$create_time,
							 );
			$datasave=Zone::insert($maindataarr);
			if($datasave)
			{
				return ['status' => 'success', 'redirect' => '', 'msg' =>'Zone Added Successfully'];
			}
			else
			{
				return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
			}
		}
    }
    public function weight()
    {
        if(session()->has('users_id'))
        {
            $data['title'] = "Weight";
            return view('vendor.weight',compact('data'));
        }
        else
        {
            return redirect()->intended('/login');
        }
    }
    public function add_weight(Request $request)
    {   
    	$weight_name=$request->get('weight_name');
    	if(!empty($request->get('weight_label_name')))
		{
			$weight_label_name=implode(',',$request->get('weight_label_name'));
		}
		else
		{
			$weight_label_name="";
		}		
    	date_default_timezone_set('Asia/Kolkata');
		$create_date=date("Y-m-d");
		$create_time=date("h:i:sa");
		$checkweight=Weight::where('weight_name',$weight_name)->first();
		if($checkweight)
		{
			return ['status' => 'exist', 'redirect' => '', 'msg' =>'Weight already exist'];
		}
		else
		{
			$maxValue = Weight::max('weight_order');
			$neworder=$maxValue + 1;
			$maindataarr=array('weight_name' => $weight_name,
								'weight_label_name'=>$weight_label_name,
								'weight_order'=>$neworder,
								'weight_status'=>'1',
								'weight_date'=>$create_date,
								'weight_time'=>$create_time,
							 );
			$datasave=Weight::insert($maindataarr);
			if($datasave)
			{
				return ['status' => 'success', 'redirect' => '', 'msg' =>'Weight Added Successfully'];
			}
			else
			{
				return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
			}
		}
    }
    public function shipping_services()
    {
        if(session()->has('users_id'))
        {
            $data['title'] = "Shipping Services";
            return view('vendor.shipping-services',compact('data'));
        }
        else
        {
            return redirect()->intended('/login');
        }
    }
    public function add_shipping_services(Request $request)
    {   
    	$ship_title=$request->get('ship_title');
    	$ship_color=$request->get('title_color');	
    	date_default_timezone_set('Asia/Kolkata');
		$create_date=date("Y-m-d");
		$create_time=date("h:i:sa");
		$checkship=ShippingServices::where('ship_title',$ship_title)->first();
		if($checkship)
		{
			return ['status' => 'exist', 'redirect' => '', 'msg' =>'Ship title already exist'];
		}
		else
		{
			$maxValue = ShippingServices::max('ship_order');
			$neworder=$maxValue + 1;
			$maindataarr=array('ship_title' => $ship_title,
								'ship_color'=>$ship_color,
								'ship_order'=>$neworder,
								'ship_status'=>'1',
								'ship_date'=>$create_date,
								'ship_time'=>$create_time,
							 );
			$datasave=ShippingServices::insert($maindataarr);
			if($datasave)
			{
				return ['status' => 'success', 'redirect' => '', 'msg' =>'Shipping Service Added Successfully'];
			}
			else
			{
				return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
			}
		}
    }
    public function product(Request $request)
    {
		$finsupc=Product::orderBy('pro_id','DESC')->first();
		if($finsupc)
		{
			$upcno=$finsupc->upcseries;
			$newupc=$upcno + 1;
		}
		else
		{
			$newupc='12345670';
		}
		$i=$newupc ;
		$i= (string)$i ;
		$len=strlen($i)-1;
		$n=strlen($i);
		$myarray=array();
		for($j=0; $j <= $len; $j++)
		{
		  
		   $myarray[]= $i[$j];
		}
		$even = 0; 
		$odd = 0; 
		for ($m = 0; $m < count($myarray); $m++)  
		{ 
			// Loop to find even, odd sum 
			if ($m % 2 == 0) 
			$even += $myarray[$m]; 
			else
			$odd += $myarray[$m]; 
		}     
		$oddsum= $odd * 3 + $even;
		$x = $oddsum;
		$lastd=0;
		while($x <= 100) 
		{
			if($x % 10==0)
			{
				break;
			}
			$lastd++;
			$x+=1;
		}
		$stringnew = substr($i, 0, -1);
		$myupcno='000'.$stringnew.$lastd;
      	$productcat=Catalog::where('cat_status','1')->orderBy('cat_order','ASC')->get();
      	$productsubdata=Subcatalog::where('scat_status','1')->orderBy('scat_order','ASC')->get();
      	$branddata=Brand::where('brand_status','1')->orderBy('brand_order','ASC')->get();
        $producttax=Tax::where('tax_status','1')->orderBy('tax_order','ASC')->get();
      	$proattiribute=Attribute::where('attribute_status','1')->where('attribute_selection','attribute')->orderBy('attribute_order','ASC')->get();
        $itemspicific=Attribute::where('attribute_status','1')->where('attribute_selection','item_specifics')->orderBy('attribute_order','ASC')->get();
        $country=Country::get();
        $weight=Weight::where('weight_status','1')->orderBy('weight_order','ASC')->get();
        $shipservice=ShippingServices::where('ship_status','1')->orderBy('ship_order','ASC')->get();
        $productdata=Product::where('pro_status','1')->orderBy('product_title','ASC')->get();
        $data['title'] = "Product";
      	return view('vendor.product',compact('data','productcat','productsubdata','branddata','proattiribute','itemspicific','producttax','country','weight','myupcno','newupc','shipservice','productdata'));
    }
    public function search_subcategory(Request $request)
    {
      $id=$request->get('id');
      $html="<option value=''>Select Sub Catalog</option>";
      $productsubdata=Subcatalog::where('cat_id',$id)->where('scat_status','1')->orderBy('scat_order','ASC')->get();
      foreach($productsubdata as $sublist)
      {
        $html.='<option value="'.$sublist->scat_id.'">'.$sublist->scat_name.'</option>';

      }
      echo $html;
    }
    public function product_image_upload(Request $request)
    {
		$file = $_FILES["filename"];
		$newimage=$request->file('filename');
		$chklen =  sizeof($newimage);
		$mainimg = $chklen - 1;
		$newimage=$request->file('filename')[$mainimg];
		$extension = $newimage->getClientOriginalExtension();
		$extensions=strtolower($extension);
		if($extensions=="jpeg"||$extensions=="jpg"||$extensions=="png"||$extensions=="bmp"||$extensions=="pdf")
		{
			$dir = 'public/product_image/';
			$filename = uniqid() . '_' . time() . '.' . $extensions;
			$newimage->move($dir, $filename);
			echo $filename;
		}
		else
		{
			echo "no";
		}
    }
    public function product_weight_show(Request $request)
    {
		$product_weight=$request->get('product_weight');
		$html="";
		$weightdata_sql=weight::where('weight_id',$product_weight)->first();
		$weightdata=explode(',',$weightdata_sql->weight_label_name);
		for($o=0;$o<count($weightdata);$o++)
		{
			$html.='<div class="col-md-6 mb-3">
							<label for="weight_value">'.$weightdata[$o].' </label>
							<input type="hidden" name="weight_label[]" value="'.$weightdata[$o].'">
							<input type="text" class="form-control" name="weight_value[]" id="weight_value">
                        </div>';
		}
		echo $html;
	}
	public function add_product(Request $request)
    {
      	$manfacture_date=$request->get('manfacture_date');
		if(!empty($manfacture_date))
		{
			$nedd=explode("-",$manfacture_date);
			$manfacture_date1=$nedd[2].'-'.$nedd[1].'-'.$nedd[0];
		}
		else
		{
			$manfacture_date1="";
		}
      	$item_data=$request->get('product_specfic');
		if(!empty($request->get('product_specfic')))
		{
			$item_count=count($request->get('product_specfic'));
		}
		else
		{
			$item_count=0;
		}
      	$attributedata=$request->get('product_attribute');
		if(!empty($request->get('product_attribute')))
		{
			$attribute_count =count($request->get('product_attribute'));
		}
		else
		{
			$attribute_count=0;
		}
		if(!empty($request->get('product_attribute')))
		{
			$product_attribute=implode(',',$request->get('product_attribute'));
		}
		else
		{
			$product_attribute="";
		}
		if(!empty($request->get('ship_services')))
		{
			$ship_services=implode(',',$request->get('ship_services'));
		}
		else
		{
			$ship_services="";
		}
		if(!empty($request->get('ship_cost')))
		{
			$ship_cost=implode(',',$request->get('ship_cost'));
		}
		else
		{
			$ship_cost="";
		}
		if(!empty($request->get('shipadditional')))
		{
			$shipadditional=implode(',',$request->get('shipadditional'));
		}
		else
		{
			$shipadditional="";
		}
		if(!empty($request->get('handling_time')))
		{
			$handling_time=implode(',',$request->get('handling_time'));
		}
		else
		{
			$handling_time="";
		}
		$mt=array();
		$st=array();
		//  $po_logoname=array();

		$image=$request->file('variation_image');


		date_default_timezone_set('Asia/Kolkata');
		$create_date=date("Y-m-d");
		$create_time=date("h:i:sa");
		if(!empty($request->get('product_brand')))
		{
		$brand=implode(',',$request->get('product_brand'));
		}
		else
		{
		$brand="";
		}
		if(!empty($request->get('product_specfic')))
		{
		$product_specfic=implode(',',$request->get('product_specfic'));
		}
		else
		{
		$product_specfic="";
		}

		if(!empty($request->get('packageup_image')))
		{
			$mainimage=implode(',',$request->get('packageup_image'));
		}
		else
		{
			$mainimage="";
		}
		if(!empty($request->get('product_discount')))
		{
			$product_discount=implode(',',$request->get('product_discount'));
		}
		else
		{
			$product_discount="";
		}
		if(!empty($request->get('discount_qty')))
		{
			$discount_qty=implode(',',$request->get('discount_qty'));
		}
		else
		{
			$discount_qty="";
		}
		if(!empty($request->get('discount')))
		{
			$discount=implode(',',$request->get('discount'));
		}
		else
		{
			$discount="";
		}
		if(!empty($request->get('product_main_discount')))
		{
			$product_main_discount=implode(',',$request->get('product_main_discount'));
		}
		else
		{
			$product_main_discount="";
		}
		if(!empty($request->get('weight_label')))
		{
			$weight_label=implode(',',$request->get('weight_label'));
		}
		else
		{
			$weight_label="";
		}
		if(!empty($request->get('weight_value')))
		{
			$weight_value=implode(',',$request->get('weight_value'));
		}
		else
		{
			$weight_value="";
		}
		if(!empty($request->get('product_related')))
		{
			$product_related=implode(',',$request->get('product_related'));
		}
		else
		{
			$product_related="";
		}
		$slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $request->get('product_title'));
		$checkcategorynew=Product::where('pro_slug',$slug)->first();
		if($checkcategorynew)
		{
			$newrand=rand(1,9);
			$slug=$slug.$newrand;
		}
		$maxValue=Product::orderBy('pro_id','DESC')->get();
		$neworder=count($maxValue) + 1;
		$insertproduct=array( 'pro_order'=>$neworder,
                            'pro_slug'=>$slug,
                            'product_title'=>$request->get('product_title'),
                            'product_short_description'=>$request->get('product_short_description'),
                            'seokey'=>$request->get('seokey'),
                            'product_tag'=>$request->get('product_tag'),
                            'metatag'=>$request->get('metatag'),
                            'product_sku'=>$request->get('product_sku'),
                            'upc_no'=>$request->get('product_upc'),
                            'product_model'=>$request->get('product_model'),
                            'product_category'=>$request->get('product_category'),
                            'product_subcategory'=>$request->get('product_subcategory'),
                            'product_category2'=>$request->get('product_category2'),
                            'product_subcategory2'=>$request->get('product_subcategory2'),
                            'product_manfacture'=>$request->get('product_manfacture'),
                            'product_brand'=>$brand,
                            'packageup_image'=>$mainimage,
                            'product_description'=>$request->get('description'),
                            'product_specfic'=>$product_specfic,
                            'cost_price'=>$request->get('cost_price'),
                            'multi_price'=>$request->get('multi_price'),
                            'discount_price'=>$request->get('discount_price'),
                            'saving_price'=>$request->get('saving_price'),
                            'product_data'=>$request->get('product_data'),
                            'regular_price'=>$request->get('regular_price'),
                            'sell_price'=>$request->get('sell_price'),
                            'tax_price'=>$request->get('tax_price'),
                            'product_inventory'=>$request->get('product_inventory'), 
                            'quantity'=>$request->get('quantity'),
                            'product_condition'=>$request->get('product_condition'),
                            'product_warning'=>$request->get('product_warning'),
                            'manfacture_date'=>$manfacture_date,
                            'manfacture_date_view'=>$manfacture_date1,
                            'product_discount'=>$product_discount,
                            'product_main_discount'=>$product_main_discount,
                            'discount'=>$discount,
                            'discount_qty'=>$discount_qty,
                            'shipping_weight'=>$request->get('shipping_weight'),
                            'shipping_length'=>$request->get('shipping_length'), 
                            'shipping_width'=>$request->get('shipping_width'), 
                            'shipping_height'=>$request->get('shipping_height'), 
                            'product_attribute'=>$product_attribute, 
                            'pro_date'=>$create_date, 
                            'pro_time'=>$create_time, 
                            'weight_name'=>$request->get('product_weight'),
                            'weight_label'=>$weight_label,
                            'weight_value'=>$weight_value,
                            'upcseries'=>$request->get('upcseries'),
                            'ship_services'=>$ship_services,
                            'ship_cost'=>$ship_cost,
                            'shipadditional'=>$shipadditional,
                            'handling_time'=>$handling_time,
                            'product_reward'=>$request->get('product_reward'),
                            'ship_country_name'=>$request->get('country_name'),
                            'ship_state_name'=>$request->get('state_name'),
                            'ship_city_name'=>$request->get('city_name'),
                            'shipstatus'=>$request->get('shipstatus'),
                            'product_related'=>$product_related,
                            'pro_status'=>'1',
                          );
		$datainsert=product::insert($insertproduct);
		$lastid = DB::getPdo()->lastInsertId();
		if($datainsert)
		{
      		return ['status' => 'success', 'redirect' => '', 'msg' =>'Product Added Successfully'];
      	}
      	else
      	{
        	return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
      	}
    }
}
