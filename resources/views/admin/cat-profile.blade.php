@extends('layouts.app')
 
 @section('content')
 <div class="container">
<h3>category list</h3>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Id</th>
            <th>Category Name</th>
            <th>cat Image</th>
            <th>Action</th>
            <!-- <th>Decline</th> -->
        <tr>
    </thead>
    <tbody>
        <tr id="rem">
            @foreach($catalog as $cat)
            <td>{{ $cat->cat_id }}</td>
            <td>{{ $cat->cat_name }}</td>
            <td><img src="{{ asset('public/product/catalog/' . $cat->cat_image) }}" /></td>
            <td>
                <a href="cat-edit/{{ $cat->cat_id }}" class="btn btn-success">edit</a>
            <!-- </td>
            <td> -->
            <button class='btn btn-danger delete' value='{{$cat->cat_id}}' style='margin-left:20px;'>Delete</button>
            <!-- <input data-cat_id="{{$cat->cat_id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $cat->cat_status ? 'checked' : '' }}> -->
            @if($cat->cat_status =='0')
            <button class='btn btn-success active' value='{{$cat->cat_id}}' id="active-{{$cat->cat_id}}" a_id='{{$cat->cat_id}}' style='margin-left:20px;'>Approve</button>
            <button class='btn btn-danger deactive' value='{{$cat->cat_id}}' style="display:none;" id="deactive-{{$cat->cat_id}}" d_id='{{$cat->cat_id}}' style='margin-left:20px;'>Decline</button>
           
            @else
            <button class='btn btn-success active' value='{{$cat->cat_id}}' style="display:none;" id="active-{{$cat->cat_id}}" a_id='{{$cat->cat_id}}' style='margin-left:20px;'>Approve</button>

            <button class='btn btn-danger deactive' value='{{$cat->cat_id}}' id="deactive-{{$cat->cat_id}}" d_id='{{$cat->cat_id}}' style='margin-left:20px;'>Decline</button>
            @endif
           
            </td>
        </tr>
    @endforeach
    </tbody>
<table>
</div>
<style>
    img{
        width: 50px;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).on("click", ".delete", function() { 
        // var $ele = $(this).parent().parent();
        var cat_id= $(this).val();

        // alert(cat_id);
        // die();
        confirm("Are You sure want to delete !");
        var url = "{{url('admin/delete')}}";
        var dltUrl = url+"/"+cat_id;
		$.ajax({
			url: dltUrl,
			type: "DELETE",
			cache: false,
			data:{
				_token:'{{ csrf_token() }}'
			},
			success: function(dataResult){
                alert(dataResult);
                $("#rem").remove();
			}
		});
	});

    $(document).on("click",".active", function(){
        var cat_id = $(this).val();
        var a_id = $(this).attr("a_id");
        var a_id1 = '#active-'+a_id;
        var d_id1 = '#deactive-'+a_id;
        var url = "{{url('admin/approve')}}";
        var appUrl = url+"/"+cat_id;
        $.ajax({
            url: appUrl,
            type: "post",
            cache:false,
            data:{
                _token:'{{ csrf_token() }}'
            },
            success: function(dataResult){
                if(dataResult=="successfully"){
        
                    $(a_id1).hide();
                    $(d_id1).show();

                }
			}
        });
    });
    $(document).on("click",".deactive", function(){
        var cat_id = $(this).val();
        var d_id = $(this).attr("d_id");
        var d_id1 = '#deactive-'+d_id;
        var a_id1 = '#active-'+d_id;
        var url = "{{url('admin/decline')}}";
        var decUrl = url+"/"+cat_id;
        $.ajax({
            url: decUrl,
            type: "post",
            cache:false,
            data:{
                _token:'{{ csrf_token() }}'
            },
            success: function(dataResult){
                if(dataResult=="successfully"){
                    $(d_id1).hide();
                    $(a_id1).show();
                }
			}
        });
    });

</script>



 @endsection