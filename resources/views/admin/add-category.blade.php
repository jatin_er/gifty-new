 @extends('layouts.app')
 
    @section('content')
     <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header start -->
                                    <div class="page-header">
                                        <div class="row align-items-end">
                                            <div class="col-lg-8">
                                                <div class="page-header-title">
                                                    <div class="d-inline">
                                                        <h4>Add Category</h4>
                                                       <!--  <span>Lorem ipsum dolor sit <code>amet</code>, consectetur
                                                            adipisicing elit</span> -->
                                                            
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="page-header-breadcrumb">
                                                    <ul class="breadcrumb-title">
                                                        <li class="breadcrumb-item"  style="float: left;">
                                                            <a href="{{url('/admin/')}}"> <i class="feather icon-home"></i> </a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Master</a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Add Category</a>
                                                        </li>
                                                    </ul>
                                                    <button class="btn btn-primary"><a href="{{ url('/admin/cat-profile')}}">profile</a></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-header end -->

                                    <!-- Page body start -->
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- Basic Form Inputs card start -->
                                                <div class="card">
                                                    
                                                    <div class="card-block">
                                                        <h4 class="sub-title">Add Category</h4>
                                                        <form id="d" enctype="multipart/form-data" >
                                                            {{csrf_field()}}
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Category Name<sup>*</sup></label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control form-control-round form-control-uppercase textfield" id="category" name="category" placeholder="Category Name" required autofocus>
                                                                     <span class="text-danger col-form-label" id="category_err" style="color:red;"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label
                                                                    class="col-sm-2 col-form-label">SEO Keywords</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text"  class="form-control form-control-round" id="seokey" name="seokey" placeholder="SEO Keywords" required>
                                                                </div>
                                                            </div>
                                                             <div class="form-group row">
                                                                <label
                                                                    class="col-sm-2 col-form-label">Meta Description</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" id="meta_description" name="meta_description"
							                                              placeholder="Write Something..."  required></textarea>
							                                    <span class="text-danger" id="meta_description_err" style="color:red;"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="category_image">Category Image*</label>
                                                                <div class="col-sm-10">
                                                                    <input type="file" class="form-control textfield" id="category_image" name="image" placeholder="Category Name" required>
                                                                    <!-- <span class="text-danger" id="category_image_err" style="color:red;"></span>
                                                                    <img id="blah" class="blah" src="#" alt="your image" style="display:none;width: 45px"  />
                                                                    <input type="hidden" class="imgda"> -->
                                                                </div>
                                                             </div>
                                                             <div class="form-group row">
                                                             <div class="col-sm-3">
                                                                    <button class="btn btn-primary" type="button" id="submit">Add</button>
                                                                </div>
                                                             </div>
                                                           
                                                        </form>
                                                        
                                                    </div>
                                                </div>
                                              
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page body end -->
                                </div>
                            </div>
                            <!-- Main-body end -->
                            <div id="styleSelector">

                            </div>
                        </div>
                    </div>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

                    <script>
                $(document).ready(function() 
            {
            $("#submit").click(function() 
            {

                var url = "{{ url('/admin/store-category') }}";
                var category = $('#category').val();
                var seokey = $('#seokey').val();
                var meta_description = $('#meta_description').val();
            var image = $('#category_image').val();
            var formData = new FormData($('#d')[0]);

            $(".error").remove();

            if (category.length<1) {
            $('#category').after('<span class="error" style="color:red">This field is required</span>');
            }
            if (seokey.length<1) {
            $('#seokey').after('<span class="error" style="color:red">This field is required</span>');
            }
            if (meta_description.length<1) {
            $('#meta_description').after('<span class="error" style="color:red">This field is required</span>');
            }
            if (image == "") {
            $('#category_image').after('<span class="error" style="color:red">This field is required</span>');
            }
                else
                // if(username!="" && email!="" && password!="" && course!="")
                {
                    $.ajax({
                    url:url,
                    type:'POST',
                    data:formData,
                    cache:false,
                contentType: false,
                processData: false,
                    success:function(data){
                        // window.location = "/employee";
                        // $('#tbl').append();
                        if(data.status=='success')
                    {
                        $('#d')[0].reset();
                        alert('successfully');
                    }else{
                        alert('category exist');
                    }
                
                          },
                    });
                }
             });
            });
</script>
    @endsection