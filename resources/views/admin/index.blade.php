@include('admin.include.header')
<!-- Pre loader -->
<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div>
<div id="app">
<main>
    <div id="primary" class="p-t-b-100 height-full">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mx-md-auto paper-card">
                    <div class="text-center">
                        <img src="{{ asset('assets/images/logo/gifty-logo.jpeg') }}" alt="">
                        <h3 class="mt-2">Welcome Back</h3>
                        <p class="p-t-b-20">Hey Admin welcome back signin now there is lot of new stuff waiting
                            for you</p>
                    </div>
                    <form id="loginform" method="post">
                        <div class="form-group has-icon"><i class="icon-envelope-o"></i>
                            <input class="form-control form-control-lg textfield" id="admin_username" name="admin_username" type="text" placeholder="Username" autofocus>
                        </div>
                        <div class="form-group has-icon"><i class="icon-user-secret"></i>
                            <input class="form-control form-control-lg textfield" id="admin_pswrd" name="admin_pswrd" type="password" placeholder="Password">
                        </div>
                        <input type="button" class="btn btn-primary btn-lg btn-block" id="loginBtn" value="Log In">
                        <!-- <p class="forget-pass">Have you forgot your username or password ?</p>  -->
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #primary -->
</main>
<div class="control-sidebar-bg shadow white fixed"></div>
</div>
@include('admin.include.footer')
<script>
    $(document).on('click','#loginBtn',function()
    {
        var isValid = true;
        $('.textfield').each(function()
        {
            if($(this).val().trim()=="")
            {
                isValid = false;        
            }
        });
        if(isValid)
        {
            var formdata='_token={{ csrf_token() }}&'+$("#loginform").serialize();
            $.ajax(
            {
                url: '{{route("admin_loginCheck")}}',
                type: 'POST',
                data: formdata,
                success: function(data) 
                {
                    if(data.status=='success')
                    {
                        Swal.fire({
                        title: "Successfully Login",
                        text: data.msg,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false
                        });
                        window.location.href="{{url('admin/dashboard')}}";
                    }
                    else
                    {
                        Swal.fire({
                        title: "Notice",
                        text: data.msg,
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        dangerMode: true,
                        });
                    }
                },
            });
        }
        else
        {
            Swal.fire({
            title: "Notice",
            text: 'Please fill all required fields',
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            cancelButtonText:false,
            closeOnConfirm: false,
            closeOnCancel: false,
            dangerMode: true,
            });
        }
    });

    $(document).on('keypress','.textfield',function(e)
    {
        if(e.which == 13) 
        {
            var isValid = true;
            $('.textfield').each(function()
            {
                if($(this).val().trim()=="")
                {
                    isValid = false;        
                }
            });
            if(isValid)
            {
                var formdata='_token={{ csrf_token() }}&'+$("#loginform").serialize();
                $.ajax(
                {
                    url: '{{route("admin_loginCheck")}}',
                    type: 'POST',
                    data: formdata,
                    success: function(data) 
                    {
                        if(data.status=='success')
                        {
                            Swal.fire({
                            title: "Successfully Login",
                            text: data.msg,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            cancelButtonText:false,
                            closeOnConfirm: false,
                            closeOnCancel: false
                            });
                            window.location.href="{{url('admin/dashboard')}}";
                        }
                        else
                        {
                            Swal.fire({
                            title: "Notice",
                            text: data.msg,
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            cancelButtonText:false,
                            closeOnConfirm: false,
                            closeOnCancel: false,
                            dangerMode: true,
                            });
                        }
                    },
                });
            }
            else
            {
                Swal.fire({
                title: "Notice",
                text: 'Please fill all required fields',
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                cancelButtonText:false,
                closeOnConfirm: false,
                closeOnCancel: false,
                dangerMode: true,
                });
            }
            return false;
        }
    });
</script>
</body>
</html>