@extends('layouts.app')
 
 @section('content')

 <div class="page-body">
    <div class="row">
        <div class="col-sm-12">
            <!-- Basic Form Inputs card start -->
            <div class="card">
                
                <div class="card-block">
                    <h4 class="sub-title">Edit Category</h4>
                    <form id="d" enctype="multipart/form-data" >
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Category Name<sup>*</sup></label>
                                <div class="col-sm-10">
                                <select id="category" name="category" class="custom-select form-control textfield" required autofocus>
                                    <option value="">Select Category Name</option>
                                    @foreach($catalog as $cat)
                                        <option value="{{$cat->cat_id}}">{{$cat->cat_name}}</option>
                                    @endforeach
                                </select>
                                <span class="text-danger" id="category_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Sub Category Name<sup>*</sup></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-round form-control-uppercase textfield" id="subcategory" name="subcategory" value="{{$subcatalog->scat_name}}" placeholder="Subcategory Name" required autofocus>
                                    <span class="text-danger col-form-label" id="category_err" style="color:red;"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label
                                class="col-sm-2 col-form-label">SEO Keywords</label>
                            <div class="col-sm-10">
                                <input type="text"  class="form-control form-control-round" id="seokey" name="seokey" value="{{$subcatalog->seokey}}" placeholder="SEO Keywords" required>
                            </div>
                        </div>
                            <div class="form-group row">
                            <label
                                class="col-sm-2 col-form-label">Meta Description</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="meta_description" name="meta_description"
                                      value="{{$subcatalog->meta_description}}"  placeholder="Write Something...">{{$subcatalog->meta_description}}</textarea>
                            <span class="text-danger" id="meta_description_err" style="color:red;"></span>
                            </div>
                        </div>
                        <input type="hidden" name="scat_id" id="scat_id" value="{{$subcatalog->scat_id}}">
                            <div class="form-group row">
                            <div class="col-sm-3">
                                <button class="btn btn-primary" type="button" id="update_data">Update</button>
                            </div>
                            </div>
                        
                    </form>
                    
                </div>
            </div>
            
        </div>
    </div>
</div>
    <style>
        img{
            width: 50px;
        }    
    </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

     <script>
        $(document).ready(function(){

    $(document).on("click", "#update_data", function() { 
        
        var category = $('#category').val();
        var subcategory = $('#subcategory').val();
        var url = "{{route('subcat_update')}}"; 
        var formData = new FormData($('#d')[0]);

        $(".error").remove();
        if (category.length<1) {
            $('#category').after('<span class="error" style="color:red">This field is required</span>');
        }
        if (subcategory.length<1) {
            $('#subcategory').after('<span class="error" style="color:red">This field is required</span>');
        }
            else
                {
                    $.ajax({
                        url: url,
                        type: "post",
                        cache: false,
                        data:formData,
                        cache:false,
                    contentType: false,
                    processData: false,
                        success:function(data){
                            if(data.status=='success')
                        {
                            // alert('successfully');
                            window.location = "/gifty/admin/subcat-list";
                        }
                        else{
                            alert("already slug exist");
                        }
                            
                        }
                    });
                }
	}); 
});
</script>
 @endsection