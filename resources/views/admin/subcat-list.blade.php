@extends('layouts.app')
 
 @section('content')
 <div class="container">
<h3>category list</h3>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Id</th>
            <th>Category Id</th>
            <th>Sub Category Name</th>
            <th>Action</th>
            <!-- <th>Decline</th> -->
        <tr>
    </thead>
    <tbody>
        <tr id="rem">
            @foreach($subcatalog as $scat)
            <td>{{ $scat->scat_id }}</td>
            <td>{{ $scat->cat_id }}</td>
            <td>{{ $scat->scat_name }}</td>
            <td>
                <a href="subcat-edit/{{ $scat->scat_id }}" class="btn btn-success">edit</a>
            <!-- </td>
            <td> -->
            <button class='btn btn-danger delete' value='{{$scat->scat_id}}' style='margin-left:20px;'>Delete</button>

            @if($scat->scat_status =='0')
            <button class='btn btn-success active' value='{{$scat->scat_id}}' id="active-{{$scat->scat_id}}" a_id='{{$scat->scat_id}}' style='margin-left:20px;'>Approve</button>
            <button class='btn btn-danger deactive' value='{{$scat->scat_id}}' style="display:none;" id="deactive-{{$scat->scat_id}}" d_id='{{$scat->scat_id}}' style='margin-left:20px;'>Decline</button>
           
            @else
            <button class='btn btn-success active' value='{{$scat->scat_id}}' style="display:none;" id="active-{{$scat->scat_id}}" a_id='{{$scat->scat_id}}' style='margin-left:20px;'>Approve</button>

            <button class='btn btn-danger deactive' value='{{$scat->scat_id}}' id="deactive-{{$scat->scat_id}}" d_id='{{$scat->scat_id}}' style='margin-left:20px;'>Decline</button>
            @endif
            </td>
        </tr>
    @endforeach
    </tbody>
<table>
</div>
<style>
    img{
        width: 50px;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).on("click", ".delete", function() { 
        // var $ele = $(this).parent().parent();
        var scat_id= $(this).val();
        // alert(scat_id);
        // die();
        confirm("Are You sure want to delete !");
        var url = "{{url('admin/subcat-delete')}}";
        var dltUrl = url+"/"+scat_id;
		$.ajax({
			url: dltUrl,
			type: "DELETE",
			cache: false,
			data:{
				_token:'{{ csrf_token() }}'
			},
			success: function(dataResult){
                // alert(dataResult);
                // $("#rem").remove();
                if(dataResult.status=='success')
                    {
                        // alert('successfully');
                        $("#rem").remove();
                    }
                    else{
                        alert("already slug exist");
                    }
			}
		});
	});

    $(document).on("click",".active", function(){
        var scat_id = $(this).val();
        var a_id = $(this).attr("a_id");
        var a_id1 = '#active-'+a_id;
        var d_id1 = '#deactive-'+a_id;
        var url = "{{url('admin/subcat_approve')}}";
        var appUrl = url+"/"+scat_id;
        $.ajax({
            url: appUrl,
            type: "post",
            cache:false,
            data:{
                _token:'{{ csrf_token() }}'
            },
            success: function(dataResult){
                if(dataResult=="successfully"){
        
                    $(a_id1).hide();
                    $(d_id1).show();

                }
			}
        });
    });
    $(document).on("click",".deactive", function(){
        var scat_id = $(this).val();
        var d_id = $(this).attr("d_id");
        var d_id1 = '#deactive-'+d_id;
        var a_id1 = '#active-'+d_id;
        var url = "{{url('admin/subcat_decline')}}";
        var decUrl = url+"/"+scat_id;
        $.ajax({
            url: decUrl,
            type: "post",
            cache:false,
            data:{
                _token:'{{ csrf_token() }}'
            },
            success: function(dataResult){
                if(dataResult=="successfully"){
                    $(d_id1).hide();
                    $(a_id1).show();
                }
			}
        });
    });
</script>



 @endsection