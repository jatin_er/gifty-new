<?php
use App\Http\Controllers\UserController;
?>
@include('users.include.header')
@include('users.include.breadcrumbs')
<div class="main-wrapper">
    <div class="shop-area pt-120 pb-120">
        <div class="container">
            <div class="row flex-row-reverse">
                <div class="col-lg-9">
                    <div class="shop-topbar-wrapper">
                        <div class="shop-topbar-left">
                            <div class="view-mode nav">
                                <a class="active" href="#shop-1" data-toggle="tab"><i class="icon-grid"></i></a>
                                <a href="#shop-2" data-toggle="tab"><i class="icon-menu"></i></a>
                            </div>
                            <p>Showing 1 - 20 of 30 results </p>
                        </div>
                        <div class="product-sorting-wrapper">
                            <div class="product-shorting shorting-style">
                                <label>View :</label>
                                <select>
                                    <option value=""> 20</option>
                                    <option value=""> 23</option>
                                    <option value=""> 30</option>
                                </select>
                            </div>
                            <div class="product-show shorting-style">
                                <label>Sort by :</label>
                                <select>
                                    <option value="">Default</option>
                                    <option value=""> Name</option>
                                    <option value=""> price</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="shop-bottom-area">
                        <div class="tab-content jump">
                            <div id="shop-1" class="tab-pane active">
                                <div class="row mydatafiltershow">
                                    @if(count($product)>0)
                                        @foreach($product as $prolist)
                                            <?php
                                            $pro_images=explode(",",$prolist->packageup_image);
                                            if(empty($pro_images))
                                            {
                                                $poimage='assets/1-9.jpg';
                                            }
                                            else
                                            {
                                                $poimage='product_image/'.$pro_images[0].'';
                                            }
                                            ?>
                                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                                                <div class="single-product-wrap mb-35">
                                                    <div class="product-img product-img-zoom mb-15">
                                                        <a href="product-details.html">
                                                            <img src="{{asset($poimage)}}" alt="{{$prolist->product_title}}">
                                                        </a>
                                                        <div class="product-action-2 tooltip-style-2">
                                                            <button title="Wishlist"><i class="icon-heart"></i></button>
                                                            <button title="Quick View" data-toggle="modal" data-target="#exampleModal"><i class="icon-size-fullscreen icons"></i></button>
                                                            <button title="Compare"><i class="icon-refresh"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="product-content-wrap-2 text-center">
                                                        <div class="product-rating-wrap">
                                                            <div class="product-rating">
                                                                <i class="icon_star"></i>
                                                                <i class="icon_star"></i>
                                                                <i class="icon_star"></i>
                                                                <i class="icon_star"></i>
                                                                <i class="icon_star gray"></i>
                                                            </div>
                                                            <span>(2)</span>
                                                        </div>
                                                        <h3><a href="product-details.html">{{$prolist->product_title}}</a></h3>
                                                        <div class="product-price-2">
                                                            <span>${{sprintf('%0.2f', $prolist->regular_price)}}</span>
                                                        </div>
                                                    </div>
                                                    <div class="product-content-wrap-2 product-content-position text-center">
                                                        <div class="product-rating-wrap">
                                                            <div class="product-rating">
                                                                <i class="icon_star"></i>
                                                                <i class="icon_star"></i>
                                                                <i class="icon_star"></i>
                                                                <i class="icon_star"></i>
                                                                <i class="icon_star gray"></i>
                                                            </div>
                                                            <span>(2)</span>
                                                        </div>
                                                        <h3><a href="product-details.html">{{$prolist->product_title}}</a></h3>
                                                        <div class="product-price-2">
                                                            <span>${{sprintf('%0.2f', $prolist->regular_price)}}</span>
                                                        </div>
                                                        <div class="pro-add-to-cart">
                                                            <button title="Add to Cart">Add To Cart</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        No Product Found
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="pro-pagination-style text-center mt-10">
                            <ul>
                                <li><a class="prev" href="#"><i class="icon-arrow-left"></i></a></li>
                                <li><a class="active" href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a class="next" href="#"><i class="icon-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="sidebar-wrapper sidebar-wrapper-mrg-right">
                        <div class="sidebar-widget mb-40">
                            <h4 class="sidebar-widget-title">Search </h4>
                            <div class="sidebar-search">
                                <form class="sidebar-search-form" action="#">
                                    <input type="text" placeholder="Search here...">
                                    <button>
                                        <i class="icon-magnifier"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div class="sidebar-widget shop-sidebar-border mb-35 pt-40">
                            <h4 class="sidebar-widget-title">Brands </h4>
                            <div class="shop-catigory">
                                <ul>
                                    @foreach($branddata as $brandlist)
                                    <li>
                                        <a href="javascript:void(0)">
                                            <input type="checkbox"  class="check-input"  style="margin-right:7px;" name="brand_id" value="{{$brandlist->brand_id}}">{{$brandlist->brand_name}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="sidebar-widget shop-sidebar-border mb-35 pt-40">
                            <h4 class="sidebar-widget-title">Categories </h4>
                            <div class="shop-catigory">
                                <ul>
                                    @foreach($product_cat as $cat_list_shop)
                                    <li>
                                        <a href="javascript:void(0)">
                                            <input type="checkbox" class="check-input"  style="margin-right:7px;" name="category_id" value="{{$cat_list_shop->cat_id}}">{{$cat_list_shop->cat_name}}
                                        </a>
                                        <?php 
                                            $srch_scat=UserController::srch_scat($cat_list_shop->cat_id); 
                                        ?>
                                        @if(count($srch_scat)>0)
                                        <!--<ul class="2nd-ul">-->
                                        <!--    @foreach($srch_scat as $srch_scats)-->
                                        <!--    <li>-->
                                        <!--        >><a href="javascript:void(0)">-->
                                        <!--            <input type="checkbox" class="check-input"  style="margin-right:7px;" name="subcategory_id" value="{{$srch_scats->scat_id}}">{{$srch_scats->scat_name}}-->
                                        <!--        </a>-->
                                        <!--    </li>-->
                                        <!--    @endforeach-->
                                        <!--</ul>-->
                                        @endif
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
    <!--                    <fieldset class="filter-price">-->
   
    <!--<div class="price-field">-->
    <!--  <input type="range" min="100" max="500" value="100" id="lower">-->
    <!--  <input type="range" min="100" max="500" value="500" id="upper">-->
    <!--</div>-->
    <!-- <div class="price-wrap">-->
    <!--  <span class="price-title">Price</span>-->
    <!--  <div class="price-wrap-1">-->
    <!--   <label for="one">$</label>-->
    <!--     <input id="one">-->
    <!--  </div>-->
    <!--  <div class="price-wrap_line">-</div>-->
    <!--  <div class="price-wrap-2">-->
    <!--      <label for="two">$</label>-->
    <!--    <input id="two">-->
    <!--  </div>-->
    <!--</div>-->
  </fieldset>
                        <div class="sidebar-widget shop-sidebar-border mb-40 pt-40">
                            <h4 class="sidebar-widget-title">Price Filter </h4>
                            <!--<div class="price-filter">-->
                                <!-- <span>Range:  $100.00 - 1.300.00 </span> -->
                            <!--    <div id="slider-range"></div>-->
                            <!--    <div class="price-slider-amount">-->
                            <!--        <div class="label-input">-->
                            <!--            <input type="text" id="amount" name="price" placeholder="Add Your Price" />-->
                            <!--        </div>-->
                                    <!-- <button type="button">Filter</button> -->
                            <!--    </div>-->
                            <!--</div>-->
                            <ul class="sidebar-checkbox_list">
                                    <li>
                                        <a href="javascript:void(0)">
                                            <input type="checkbox" style="margin-right:7px;">$10-$20</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <input type="checkbox" style="margin-right:7px;">$20-$50</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <input type="checkbox" style="margin-right:7px;">$50-$100</a>
                                    </li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('users.include.footer')
<script>
        var lowerSlider = document.querySelector('#lower');
var  upperSlider = document.querySelector('#upper');

document.querySelector('#two').value=upperSlider.value;
document.querySelector('#one').value=lowerSlider.value;

var  lowerVal = parseInt(lowerSlider.value);
var upperVal = parseInt(upperSlider.value);

upperSlider.oninput = function () {
    lowerVal = parseInt(lowerSlider.value);
    upperVal = parseInt(upperSlider.value);

    if (upperVal < lowerVal + 4) {
        lowerSlider.value = upperVal - 4;
        if (lowerVal == lowerSlider.min) {
        upperSlider.value = 4;
        }
    }
    document.querySelector('#two').value=this.value
};

lowerSlider.oninput = function () {
    lowerVal = parseInt(lowerSlider.value);
    upperVal = parseInt(upperSlider.value);
    if (lowerVal > upperVal - 4) {
        upperSlider.value = lowerVal + 4;
        if (upperVal == upperSlider.max) {
            lowerSlider.value = parseInt(upperSlider.max) - 4;
        }
    }
    document.querySelector('#one').value=this.value
};
    </script>
<script>
    $(document).on('change',"input[name='brand_id'],input[name='category_id'],input[name='subcategory_id']",function()
    {
        var brand=[];
        var cat_id=[];
        var subcat_id=[];
        $("input:checkbox[name=brand_id]:checked").each(function()
        {
            brand.push($(this).val());
        });

        $("input:checkbox[name=category_id]:checked").each(function()
        {
            cat_id.push($(this).val());
        });
        $("input:checkbox[name=subcategory_id]:checked").each(function()
        {
            subcat_id.push($(this).val());
        });
        var tbrand=brand.join(",");
        var tcat_id=cat_id.join(",");
        var tsubcate_id=subcat_id.join(",");
        $.ajax(
        {
            url:"{{ route('searchproduct_filter')}}",
            type:"GET",
            data:{
                'tbrand':tbrand,
                'tcat_id':tcat_id,
                'tsubcate_id':tsubcate_id,
              },
            success:function(response)
            {
                alert(response);
                 $('.mydatafiltershow').html(response);
            }
        });
    });
</script>