@include('users.include.header')
@include('users.include.breadcrumbs')
<style>
    
.login-register-wrapper .login-form-container .login-register-form form select{
    margin-bottom: 10px !important;
}
.btn{
    font-size: 12px !important;
    padding: 5px 3px 5px 3px !important;
}
.select-option{
    display: flex;
   justify-content: space-between;
}
.fb-icon-l{
    /*display: flex;*/
    /*justify-content: center;*/
    /*align-items: center;*/
}
.login-register-wrapper .login-form-container .login-register-form form input[type="radio"]{
        margin-right: 10px;
}
.side-icon{
    position: absolute;
    right: 112px;
    margin-top: 18px;
}
.p-1 {
    padding: 0rem !important;
}
@media screen and (max-width: 786px){
    .side-icon{
    position: absolute;
    right: 50px;
    margin-top: 18px;
}
.btn{
    font-size: 16px !important;
    padding: 5px 3px 5px 3px !important;
    width: 100% !important;
    margin-bottom: 7px !important;
}
.s-icon{
   margin: 0px 14px 0px 14px !important;
}
}
</style>
<div class="login-register-area pt-115 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 ml-auto mr-auto">
                <div class="login-register-wrapper">
                    <div class="login-register-tab-list nav">
                        <a class="active" data-toggle="tab" href="#lg1">
                            <h4> login </h4>
                        </a>
                        <a data-toggle="tab" href="#lg2">
                            <h4> register </h4>
                        </a>
                    </div>
                    <div class="tab-content">
                        <div id="lg1" class="tab-pane active">
                            <div class="login-form-container">
                                <div class="login-register-form">
                                    <form method="post" id="login_form">
                                        <input type="text" name="emails" id="emails" class="textfield1" placeholder="Email">
                                        <input type="password" name="passwords" id="passwords" class="textfield1" placeholder="Password" required="">
                                        <div class="button-box">
                                            <!-- <div class="login-toggle-btn">
                                                <input type="checkbox">
                                                <label>Remember me</label>
                                                <a href="#">Forgot Password?</a>
                                            </div> -->
                                            <button type="button" id="login-form" name="submit">Login</button>
                                        </div>
                                        <div class="row" style="margin-top:15px;">
                                        <div class="col-sm-6">
                                        <div class="btn btn-primary">
                                            <a href="{{url('/redirect')}}" class="fb btn">
                                            <div class="fb-icon-l">
                                                <i class="fa fa-fw fa-facebook" style="margin-right:5px; color: white;"></i>
                                                 <span style="color: white;">Continue with Facebook</span>
                                            </div>
                                           
                                            </a>
                                        </div>
                                        </div>
                                        <div class="col-sm-6 s-icon p-1">
                                       <div class="btn btn-warning">
                                        <a href="{{url('/redirectgmail')}}" class="google btn" style="margin-right: 30px;">
                                          <div class="fb-icon-l">
                                            <i class="fa fa-fw fa-google" style="margin-right:5px; color: white;"></i>
                                             <span style =" color: white;">Continue with Google</span>
                                          </div>
                                         
                                        </a>
                                        </div>
                                          </div>
                                          </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="lg2" class="tab-pane">
                            <div class="login-form-container">
                                <div class="login-register-form">
                                    <form method="post" id="add_form">
                                        <select name="country_id" id="country_id" class="textfield">
                                            <option value="">Select Country/Region</option>
                                            @foreach($countries as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="select-option">
                                        <label for="supplier">
                                        <input type="radio" id="supplier" name="i_am_a" value="supplier">
                                        Supplier</label>
                                        
                                        <label for="buyer">
                                        <input type="radio" id="buyer" name="i_am_a" value="buyer">
                                        Buyer</label>
                                        <label for="both">
                                        <input type="radio" id="both" name="i_am_a" value="both">
                                        Both</label> 
                                        </div>
                                        <span class="text-danger" id="country_id_err" style="color:red;"></span>
                                        
                                        <div class="form-group">
                                        <input type="text" name="name" id="name" class="textfield" placeholder="First Name">
                                        <span class="text-danger" id="i_am_a_err" style="color:red;"></span>
                                        </div>
                                        
                                        <div class="form-group">
                                        <input type="text"  name="last_name" id="last_name"  class="textfield" placeholder="Last Name">
                                        <span class="text-danger" id="name_err" style="color:red;"></span>
                                        </div>
                                       
                                       <div class="form-group">
                                        <input type="text" name="company_name" id="company_name" class="textfield" placeholder="Company Name">
                                        <span class="text-danger" id="last_name_err" style="color:red;"></span>
                                        </div>
                                        
                                        <div class="form-group">
                                        <input type="text"  name="phone" id="phone" class="textfield" maxlength="10" placeholder="Phone">
                                        <i class="text-danger" id="company_name_err" style="color:red;"></i>
                                        </div>
                                        
                                        <div class="form-group">
                                        <input type="text" name="email" id="email" class="textfield" placeholder="Email Address">
                                        <span class="text-danger" id="phone_err" style="color:red;"></span>
                                        </div>
                                      
                                        <div class="input-group-append form-group">
                                        <input type="password"  name="password" id="password" class="textfield" placeholder="Password">
                                        <span toggle="#password"  class="fa fa-fw fa-eye  field-icon toggle-password side-icon"></span> 
                                        </div>
                                        <span class="text-danger" id="email_err" style="color:red;"></span>
                                        
                                        <div class="input-group-append form-group">
                                        <input type="password" name="conf_pwd" id="conf_pwd" class="textfield" placeholder="Confirm Password">
                                        <span toggle="#conf_pwd"  class="fa fa-fw fa-eye  field-icon toggle-password side-icon"></span>
                                        </div>
                                        <span class="text-danger" id="password_err" style="color:red;"></span>
                                        
                                        <div class="form-group">
                                        <input type="text"  name="captcha" id="captcha" class="textfield" placeholder="Captcha">
                                        <span class="text-danger" id="conf_pwd_err" style="color:red;"></span>
                                        </div>
                                        
                                        <div class="input-group-append form-group">
                                        <?php $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; ?>
                                        <input type="text" name="captcha1" id="captcha1" readonly value="<?php echo substr(str_shuffle($permitted_chars), 0, 6); ?>" oncopy="return false" onpaste="return false" oncut="return false">
                                        <span type="button" name="btnRefresh" id="btnRefresh" class="fa fa-fw fa-refresh side-icon"></span>
                                        </div>
                                        <span class="text-danger" id="captcha_err" style="color:red;"></span>
                                        <div class="button-box">
                                            <button type="button" id="submit">Register</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('users.include.footer')
<script type="text/javascript">
$(document).on('click','#login-form',function()
{
    var isValid = true;
    $('.textfield1').each(function()
    {
        if($(this).val().trim()=="")
        {
            isValid = false;        
        }
    });
    if(isValid)
    {
        var formdata=$("#login_form").serialize()+"&_token={{ csrf_token() }}";
        $.ajax(
        {
            url: '{{route("login_user")}}',
            type: "POST",
            data: formdata,
            success: function(data) 
            {
                if(data.status=='success')
                {
                    Swal.fire({
                    title: "Successfully Login",
                    text: data.msg,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    cancelButtonText:false,
                    closeOnConfirm: false,
                    closeOnCancel: false
                    });
                    window.location.href='{{url("/")}}';
                }
                else
                {
                    Swal.fire({
                    title: "Notice",
                    text: data.msg,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    cancelButtonText:false,
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    dangerMode: true,
                    });
                }
            },
        });
    }
    else
    {
        Swal.fire({
        title: "Notice",
        text: 'Please fill all required fields',
        type: "warning",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        cancelButtonText:false,
        closeOnConfirm: false,
        closeOnCancel: false,
        dangerMode: true,
        });
    }
});
$(document).on('click','.btnRefresh',function()
{
    $.ajax(
    {
        url: '{{route("captchacode")}}',
        type: "POST",
        data: 
        {
            "_token": "{{ csrf_token() }}",
        },
        success: function(data) 
        {
            $('#captcha1').val(data);
        }
    });
});
$(document).on('blur','.textfield',function()
{
    if($(this).val().trim()=="")
    {
        $("#"+this.id+"_err").text("Please fill this field");
        $("#"+this.id+"_err").show();
    }
    else
    {
        var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if(this.id=="phone" && $(this).val().trim().length<10)
        {
            $("#"+this.id+"_err").text("Phone should be of 10-digits");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="phone" && $(this).val().trim().length>=10)
        {
            var formdata="phone="+$('#phone').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("phone_check")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#phone_err").text("Phone already exist");
                        $("#phone_err").show();
                    }
                    else
                    {
                        $("#phone_err").text("");
                        $("#phone_err").hide();
                    }
                }
            });
        }
        else if(this.id=="email" && !filter.test($(this).val().trim()))
        {
            $("#"+this.id+"_err").text("Invalid Email");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="email" && filter.test($(this).val().trim()))
        {
            var formdata="email="+$('#email').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("email_check")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#email_err").text("Email already exist");
                        $("#email_err").show();
                    }
                    else
                    {
                        $("#email_err").text("");
                        $("#email_err").hide();
                    }
                }
            });
        }
        else if(this.id=="password" && $('#conf_pwd').val().trim()!='' && $('#'+this.id).val().trim()!=$('#conf_pwd').val().trim())
        {
            $("#"+this.id+"_err").text("Password should be match with confirm password");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="conf_pwd" && $('#password').val().trim()!='' && $('#'+this.id).val().trim()!=$('#password').val().trim())
        {
            $("#"+this.id+"_err").text("Password should be match with confirm password");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="captcha" && $('#'+this.id).val().trim()!=$('#captcha1').val().trim())
        {
            $("#"+this.id+"_err").text("Invalid Captcha");
            $("#"+this.id+"_err").show();
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    }
});
$(document).on('keyup change','.textfield',function()
{
    if($(this).val().trim()=="")
    {
        $("#"+this.id+"_err").text("Please fill this field");
        $("#"+this.id+"_err").show();
    }
    else
    {
        var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if(this.id=="phone" && $(this).val().trim().length<10)
        {
            $("#"+this.id+"_err").text("Phone should be of 10-digits");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="phone" && $(this).val().trim().length>=10)
        {
            var formdata="phone="+$('#phone').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("phone_check")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#phone_err").text("Phone already exist");
                        $("#phone_err").show();
                    }
                    else
                    {
                        $("#phone_err").text("");
                        $("#phone_err").hide();
                    }
                }
            });
        }
        else if(this.id=="email" && !filter.test($(this).val().trim()))
        {
            $("#"+this.id+"_err").text("Invalid Email");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="email" && filter.test($(this).val().trim()))
        {
            var formdata="email="+$('#email').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("email_check")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#email_err").text("Email already exist");
                        $("#email_err").show();
                    }
                    else
                    {
                        $("#email_err").text("");
                        $("#email_err").hide();
                    }
                }
            });
        }
        else if(this.id=="password" && $('#conf_pwd').val().trim()!='' && $('#'+this.id).val().trim()!=$('#conf_pwd').val().trim())
        {
            $("#"+this.id+"_err").text("Password should be match with confirm password");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="conf_pwd" && $('#password').val().trim()!='' && $('#'+this.id).val().trim()!=$('#password').val().trim())
        {
            $("#"+this.id+"_err").text("Password should be match with confirm password");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="captcha" && $('#'+this.id).val().trim()!=$('#captcha1').val().trim())
        {
            $("#"+this.id+"_err").text("Invalid Captcha");
            $("#"+this.id+"_err").show();
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    }
});
$(document).on('click','#submit',function()
{
    $('.text-danger').hide();
    $('.text-danger').val('');
    var isValid = true;
    $('.textfield').each(function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
            isValid = false;        
        }
        else
        {
            var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if(this.id=="phone" && $(this).val().trim().length<10)
            {
                $("#"+this.id+"_err").text("Phone should be of 10-digits");
                $("#"+this.id+"_err").show();
            }
            if(this.id=="email" && !filter.test($(this).val().trim()))
            {
                $("#"+this.id+"_err").text("Invalid Email");
                $("#"+this.id+"_err").show();
            }
            if(this.id=="password" && $('#conf_pwd').val().trim()!='' && $('#'+this.id).val().trim()!=$('#conf_pwd').val().trim())
            {
                $("#"+this.id+"_err").text("Password should be match with confirm password");
                $("#"+this.id+"_err").show();
            }
            if(this.id=="conf_pwd" && $('#password').val().trim()!='' && $('#'+this.id).val().trim()!=$('#password').val().trim())
            {
                $("#"+this.id+"_err").text("Password should be match with confirm password");
                $("#"+this.id+"_err").show();
            }
            if(this.id=="captcha" && $('#'+this.id).val().trim()!=$('#captcha1').val().trim())
            {
                $("#"+this.id+"_err").text("Invalid Captcha");
                $("#"+this.id+"_err").show();
            }
        }
    });
    if ($('input[name="i_am_a"]:checked').length == 0)
    {
        $("#i_am_a_err").text("Please select this field");
        $("#i_am_a_err").show();
        isValid = false;
    }
    if(isValid)
    {
        var formdata=$("#add_form").serialize()+"&_token={{ csrf_token() }}";
        $.ajax(
        {
            url: '{{route("register_user")}}',
            type: "POST",
            data: formdata,
            success: function(data) 
            {
                if(data.status=='success')
                {
                    Swal.fire({
                    title: "Successfully Added",
                    text: data.msg,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    cancelButtonText:false,
                    closeOnConfirm: false,
                    closeOnCancel: false
                    });
                    window.location.href='{{url("register-login")}}';
                }
                else
                {
                    Swal.fire({
                    title: "Notice",
                    text: data.msg,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    cancelButtonText:false,
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    dangerMode: true,
                    });
                }
            },
        });
    }
    else
    {
        Swal.fire({
        title: "Notice",
        text: 'Please fill all required fields',
        type: "warning",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        cancelButtonText:false,
        closeOnConfirm: false,
        closeOnCancel: false,
        dangerMode: true,
        });
    }
});
$(function()
{
    $("#phone").keypress(function(event) 
    {
        return /\d/.test(String.fromCharCode(event.keyCode));
    });
});
$(".toggle-password").click(function() 
{
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") 
    {
        input.attr("type", "text");
    } 
    else 
    {
        input.attr("type", "password");
    }
});
</script>
</body>
</html>