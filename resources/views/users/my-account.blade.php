@include('users.include.header')
@include('users.include.breadcrumbs')
<!-- my account wrapper start -->
<style>
    .radio{
        height: 20px !important;
        position: absolute;
    }
    select{
        border: 1px solid #eceff8;
        padding: 12px;
    }
     .btn-style{
            justify-content: center;
            display: flex;
            align-items: center;
           
        }
        .myaccount-content .account-details-form .single-input-item button{
             margin-top: 15px !important;
        }
    @media screen and (max-width: 786px){
        .radio {
            left: 111px;
        }
        .btn-style{
            justify-content: center;
            display: flex;
            align-items: center;
        }
    }
</style>
<div class="my-account-wrapper pt-120 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- My Account Page Start -->
                <div class="myaccount-page-wrapper">
                    <!-- My Account Tab Menu Start -->
                    <div class="row">
                        <div class="col-lg-3 col-md-4">
                            <div class="myaccount-tab-menu nav" role="tablist">
                                <a href="#dashboad" class="active" data-toggle="tab"><i class="fa fa-dashboard"></i>
                                    Dashboard</a>
                                <a href="#orders" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i> Orders</a>
                                <a href="#download" data-toggle="tab"><i class="fa fa-cloud-download"></i> Download</a>
                                <a href="#payment-method" data-toggle="tab"><i class="fa fa-credit-card"></i> Payment
                                    Method</a>
                                <a href="#address-edit" data-toggle="tab"><i class="fa fa-map-marker"></i> address</a>
                                <a href="#account-info" data-toggle="tab"><i class="fa fa-user"></i> Account Details</a>
                                <a href="{{url('logout')}}"><i class="fa fa-sign-out"></i> Logout</a>
                            </div>
                        </div>
                        <!-- My Account Tab Menu End -->
                        <!-- My Account Tab Content Start -->
                        <div class="col-lg-9 col-md-8">
                            <div class="tab-content" id="myaccountContent">
                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Dashboard</h3>
                                        <div class="welcome">
                                            <p>Hello, <strong class="namecls">{{$userdata->name}}</strong> (If Not <strong class="namecls">{{$userdata->name}} !</strong><a href="{{url('logout')}}" class="logout"> Logout</a>)</p>
                                        </div>

                                        <p class="mb-0">From your account dashboard. you can easily check & view your recent orders, manage your shipping and billing addresses and edit your password and account details.</p>
                                    </div>
                                </div>
                                <!-- Single Tab Content End -->
                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade" id="orders" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Orders</h3>
                                        <div class="myaccount-table table-responsive text-center">
                                            <table class="table table-bordered">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th>Order</th>
                                                        <th>Date</th>
                                                        <th>Status</th>
                                                        <th>Total</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Aug 22, 2018</td>
                                                        <td>Pending</td>
                                                        <td>$3000</td>
                                                        <td><a href="cart.html" class="check-btn sqr-btn ">View</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>July 22, 2018</td>
                                                        <td>Approved</td>
                                                        <td>$200</td>
                                                        <td><a href="cart.html" class="check-btn sqr-btn ">View</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>June 12, 2017</td>
                                                        <td>On Hold</td>
                                                        <td>$990</td>
                                                        <td><a href="cart.html" class="check-btn sqr-btn ">View</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single Tab Content End -->
                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade" id="download" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Downloads</h3>
                                        <div class="myaccount-table table-responsive text-center">
                                            <table class="table table-bordered">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>Date</th>
                                                        <th>Expire</th>
                                                        <th>Download</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Haven - Free Real Estate PSD Template</td>
                                                        <td>Aug 22, 2018</td>
                                                        <td>Yes</td>
                                                        <td><a href="#" class="check-btn sqr-btn "><i class="fa fa-cloud-download"></i> Download File</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>HasTech - Profolio Business Template</td>
                                                        <td>Sep 12, 2018</td>
                                                        <td>Never</td>
                                                        <td><a href="#" class="check-btn sqr-btn "><i class="fa fa-cloud-download"></i> Download File</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single Tab Content End -->
                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade" id="payment-method" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Payment Method</h3>
                                        <p class="saved-message">You Can't Saved Your Payment Method yet.</p>
                                    </div>
                                </div>
                                <!-- Single Tab Content End -->
                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade" id="address-edit" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Billing Address</h3>
                                        <address>
                                            <p><strong>Alex Tuntuni</strong></p>
                                            <p>1355 Market St, Suite 900 <br>
                                        San Francisco, CA 94103</p>
                                            <p>Mobile: (123) 456-7890</p>
                                        </address>
                                        <a href="#" class="check-btn sqr-btn "><i class="fa fa-edit"></i> Edit Address</a>
                                    </div>
                                </div>
                                <!-- Single Tab Content End -->
                                <!-- Single Tab Content Start -->
                                <div class="tab-pane fade" id="account-info" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Account Details</h3>
                                        <div class="account-details-form">
                                            <form id="add_form" method="POST">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="single-input-item">
                                                            <label for="country_id" class="required">First Name</label>
                                                            <select class="textfield" name="country_id" id="country_id" autofocus>
                                                                <option value="">Select Country/Region</option>
                                                                @foreach($countries as $value)
                                                                    @if($value->id==$userdata->country_id)
                                                                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                                                                    @else
                                                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="single-input-item">
                                                            <label for="i_am_a" class="required">I am a</label>
                                                            <hr>
                                                            @if($userdata->i_am_a=="supplier")
                                                                <input type="radio" class="radio" id="supplier" name="i_am_a" value="supplier" checked>
                                                            @else
                                                                <input type="radio" class="radio" id="supplier" name="i_am_a" value="supplier"> 
                                                            @endif
                                                            <label for="supplier">Supplier</label>
                                                            @if($userdata->i_am_a=="buyer")
                                                                <input type="radio" id="buyer" class="radio" name="i_am_a" value="buyer" checked>
                                                            @else
                                                                <input type="radio" id="buyer" class="radio" name="i_am_a" value="buyer">
                                                            @endif
                                                            <label for="buyer">Buyer</label>
                                                            @if($userdata->i_am_a=="both")
                                                                <input type="radio" id="both" class="radio" name="i_am_a" value="both" checked>
                                                            @else
                                                                <input type="radio" id="both" class="radio" name="i_am_a" value="both">
                                                            @endif
                                                            <label for="both">Both</label>
                                                            <span class="text-danger" id="i_am_a_err" style="color:red;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="single-input-item">
                                                            <label for="name" class="required">First Name</label>
                                                            <input type="text" name="name" id="name" class="textfield" placeholder="First Name" value="{{$userdata->name}}">
                                                            <span class="text-danger" id="name_err" style="color:red;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="single-input-item">
                                                            <label for="last_name" class="required">Last Name</label>
                                                            <input type="text"  name="last_name" id="last_name"  class="textfield" placeholder="Last Name" value="{{$userdata->last_name}}">
                                                            <span class="text-danger" id="last_name_err" style="color:red;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="single-input-item">
                                                    <label for="company_name" class="required">Company Name</label>
                                                    <input type="text"  name="company_name" id="company_name" class="textfield" placeholder="Company Name" value="{{$userdata->company_name}}">
                                                    <span class="text-danger" id="company_name_err" style="color:red;"></span>
                                                </div>
                                                <div class="single-input-item">
                                                    <label for="phone" class="required">Phone</label>
                                                    <input type="text"  name="phone" id="phone" class="textfield" maxlength="10" placeholder="Phone" value="{{$userdata->phone}}">
                                                    <span class="text-danger" id="phone_err" style="color:red;"></span>
                                                </div>
                                                <div class="single-input-item">
                                                    <label for="email" class="required">Email</label>
                                                    <input type="text" name="email" id="email" class="textfield" placeholder="Email Address" value="{{$userdata->email}}">
                                                    <span class="text-danger" id="email_err" style="color:red;"></span>
                                                </div>
                                                <div class="single-input-item">
                                                    <div class="btn-style">
                                                    <button type="button" id="submit" class="check-btn sqr-btn">Save Changes</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div> <!-- Single Tab Content End -->
                            </div>
                        </div> <!-- My Account Tab Content End -->
                    </div>
                </div> <!-- My Account Page End -->
            </div>
        </div>
    </div>
</div>
<!-- my account wrapper end -->
@include('users.include.footer')
<script>
$(document).on('blur','.textfield',function()
{
    if($(this).val().trim()=="")
    {
        $("#"+this.id+"_err").text("Please fill this field");
        $("#"+this.id+"_err").show();
    }
    else
    {
        var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if(this.id=="phone" && $(this).val().trim().length<10)
        {
            $("#"+this.id+"_err").text("Phone should be of 10-digits");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="phone" && $(this).val().trim().length>=10)
        {
            var formdata="phone="+$('#phone').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("phone_check1")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#phone_err").text("Phone already exist");
                        $("#phone_err").show();
                    }
                    else
                    {
                        $("#phone_err").text("");
                        $("#phone_err").hide();
                    }
                }
            });
        }
        else if(this.id=="email" && !filter.test($(this).val().trim()))
        {
            $("#"+this.id+"_err").text("Invalid Email");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="email" && filter.test($(this).val().trim()))
        {
            var formdata="email="+$('#email').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("email_check1")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#email_err").text("Email already exist");
                        $("#email_err").show();
                    }
                    else
                    {
                        $("#email_err").text("");
                        $("#email_err").hide();
                    }
                }
            });
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    }
});
$(document).on('keyup change','.textfield',function()
{
    if($(this).val().trim()=="")
    {
        $("#"+this.id+"_err").text("Please fill this field");
        $("#"+this.id+"_err").show();
    }
    else
    {
        var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if(this.id=="phone" && $(this).val().trim().length<10)
        {
            $("#"+this.id+"_err").text("Phone should be of 10-digits");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="phone" && $(this).val().trim().length>=10)
        {
            var formdata="phone="+$('#phone').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("phone_check1")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#phone_err").text("Phone already exist");
                        $("#phone_err").show();
                    }
                    else
                    {
                        $("#phone_err").text("");
                        $("#phone_err").hide();
                    }
                }
            });
        }
        else if(this.id=="email" && !filter.test($(this).val().trim()))
        {
            $("#"+this.id+"_err").text("Invalid Email");
            $("#"+this.id+"_err").show();
        }
        else if(this.id=="email" && filter.test($(this).val().trim()))
        {
            var formdata="email="+$('#email').val().trim()+"&_token={{ csrf_token() }}";
            $.ajax(
            {
                url: '{{route("email_check1")}}',
                type: "POST",
                data:formdata,
                success: function(data) 
                {
                    if(data=="exist")
                    {
                        $("#email_err").text("Email already exist");
                        $("#email_err").show();
                    }
                    else
                    {
                        $("#email_err").text("");
                        $("#email_err").hide();
                    }
                }
            });
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    }
});
$(document).on('click','#submit',function()
{
    $('.text-danger').hide();
    $('.text-danger').val('');
    var isValid = true;
    $('.textfield').each(function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
            isValid = false;        
        }
        else
        {
            var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if(this.id=="phone" && $(this).val().trim().length<10)
            {
                $("#"+this.id+"_err").text("Phone should be of 10-digits");
                $("#"+this.id+"_err").show();
            }
            if(this.id=="email" && !filter.test($(this).val().trim()))
            {
                $("#"+this.id+"_err").text("Invalid Email");
                $("#"+this.id+"_err").show();
            }
        }
    });
    if ($('input[name="i_am_a"]:checked').length == 0)
    {
        $("#i_am_a_err").text("Please select this field");
        $("#i_am_a_err").show();
        isValid = false;
    }
    if(isValid)
    {
        var formdata=$("#add_form").serialize()+"&_token={{ csrf_token() }}";
        $.ajax(
        {
            url: '{{route("my_account_user")}}',
            type: "POST",
            data: formdata,
            success: function(data) 
            {
                if(data.status=='success')
                {
                    Swal.fire({
                    title: "Successfully Updated",
                    text: data.msg,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    cancelButtonText:false,
                    closeOnConfirm: false,
                    closeOnCancel: false
                    });
                    $('.namecls').text($('#name').val().trim());
                }
                else
                {
                    Swal.fire({
                    title: "Notice",
                    text: data.msg,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    cancelButtonText:false,
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    dangerMode: true,
                    });
                }
            },
        });
    }
    else
    {
        Swal.fire({
        title: "Notice",
        text: 'Please fill all required fields',
        type: "warning",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        cancelButtonText:false,
        closeOnConfirm: false,
        closeOnCancel: false,
        dangerMode: true,
        });
    }
});
$(function()
{
    $("#phone").keypress(function(event) 
    {
        return /\d/.test(String.fromCharCode(event.keyCode));
    });
});
</script>