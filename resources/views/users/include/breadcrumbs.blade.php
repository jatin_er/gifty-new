<div class="breadcrumb-area bg-gray">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <ul>
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>
                @if(!empty($data))
                <li class="active">{{$data['title']}}</li>
                @endif
            </ul>
        </div>
    </div>
</div>