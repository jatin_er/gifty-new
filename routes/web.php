<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\MediaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/confirmation', function () {
    return view('confirmation');
});
 Route::get('confirmation', function()
    {
        return View::make('confirmation');
    });

Route::get('/',[UserController::class, 'index']);
//Register//
Route::get('/srch_vendor',[UserController::class, 'srch_vendor'])->name('srch_vendor');
Route::get('/register-login',[UserController::class, 'register_login'])->name('register_login');
Route::get('/register',[UserController::class, 'register'])->name('register');
Route::post('/captchacode',[UserController::class, 'captchacode'])->name('captchacode');
Route::post('/email_check',[UserController::class, 'email_check'])->name('email_check');
Route::post('/phone_check',[UserController::class, 'phone_check'])->name('phone_check');
Route::post('/register_user',[UserController::class, 'register_user'])->name('register_user');
//Login//
Route::get('/login',[UserController::class, 'login'])->name('login');
Route::post('/login_user',[UserController::class, 'login_user'])->name('login_user');
Route::get('/redirect', [UserController::class, 'redirect']);
Route::get('/callback', [UserController::class, 'callback']);
Route::get('/redirectgmail', [UserController::class, 'redirectgmail']);
Route::get('/callbackgmail', [UserController::class, 'callbackgmail']);
// Route::get('/redirectlinkedin', [UserController::class, 'redirectlinkedin']);
// Route::get('/callbacklinkedin', [UserController::class, 'callbacklinkedin']);
//My Account//
Route::get('/my-account',[UserController::class, 'my_account'])->name('my_account');
Route::post('/email_check1',[UserController::class, 'email_check1'])->name('email_check1');
Route::post('/phone_check1',[UserController::class, 'phone_check1'])->name('phone_check1');
Route::post('/my_account_user',[UserController::class, 'my_account_user'])->name('my_account_user');
//Catalog//
Route::get('/catalog',[VendorController::class, 'catalog'])->name('catalog');
Route::post('/add_catalog',[VendorController::class, 'add_catalog'])->name('add_catalog');
//SubCatalog//
Route::get('/sub-catalog',[VendorController::class, 'sub_catalog'])->name('sub_catalog');
Route::post('/add_subcatalog',[VendorController::class, 'add_subcatalog'])->name('add_subcatalog');
//Tax//
Route::get('/tax',[VendorController::class, 'tax'])->name('tax');
Route::post('/add_tax',[VendorController::class, 'add_tax'])->name('add_tax');
//Zone//
Route::get('/zone',[VendorController::class, 'zone'])->name('zone');
Route::post('/add_zone',[VendorController::class, 'add_zone'])->name('add_zone');
Route::get('/search_state',[VendorController::class, 'search_state'])->name('search_state');
Route::get('/search_city',[VendorController::class, 'search_city'])->name('search_city');
//Weigth//
Route::get('/weight',[VendorController::class, 'weight'])->name('weight');
Route::post('/add_weight',[VendorController::class, 'add_weight'])->name('add_weight');
//Shipping Services//
Route::get('/shipping-services',[VendorController::class, 'shipping_services'])->name('shipping_services');
Route::post('/add_shipping_services',[VendorController::class, 'add_shipping_services'])->name('add_shipping_services');
//Product//
Route::get('/product',[VendorController::class, 'product'])->name('product');
Route::get('/search-subcategory', [VendorController::class, 'search_subcategory'])->name('search-subcategory');
Route::post('/product_image_upload', [VendorController::class, 'product_image_upload'])->name('product_image_upload');
Route::get('/product_weight_show', [VendorController::class, 'product_weight_show'])->name('product_weight_show');
Route::post('/add_product',[VendorController::class, 'add_product'])->name('add_product');
//Shop//
Route::get('/shop',[UserController::class, 'shop'])->name('shop');


Route::get('/srch_scat',[UserController::class, 'srch_scat'])->name('srch_scat');
Route::get('/searchproduct_filter/',[UserController::class, 'searchproduct_filter'])->name('searchproduct_filter');
//Logout//
Route::get('/logout',[UserController::class, 'logout'])->name('logout');

//cartpage
Route::get('/cart',[UserController::class, 'cart'])->name('cart');
Route::get('/checkout',[UserController::class, 'checkout'])->name('checkout');
Route::get('/shop-fullwide',[UserController::class, 'shop_fullwide'])->name('shop-fullwide');
Route::get('/product-details',[UserController::class, 'product_details'])->name('product-details');


Route::group(['prefix' => 'admin'], function()
{
// 	Route::get('/add-category', function () {
//     return view('admin.add-category');
// });

Route::get('/add-category',[AdminController::class, 'create'])->name('create');
Route::post('/store-category',[AdminController::class, 'store_category'])->name('store_category');
Route::get('/cat-profile',[AdminController::class, 'profile'])->name('profile');
Route::get('/cat-edit/{cat_id}', [AdminController::class, 'edit']);
Route::post('cat_update', [AdminController::class, 'cat_update'])->name('cat_update');
Route::delete('delete/{cat_id}', [AdminController::class, 'destroy'])->name('destroy');
Route::get('/changeStatus',[AdminController::class, 'changeCategoryStatus'])->name('changeCategoryStatus');
Route::post('approve/{cat_id}', [AdminController::class, 'approve'])->name('approve');
Route::post('decline/{cat_id}', [AdminController::class, 'decline'])->name('decline');

Route::get('/add-subcategory',[AdminController::class, 'add_scategory'])->name('add_scategory');
Route::post('/store-subcategory',[AdminController::class, 'store_subcategory'])->name('store_subcategory');
Route::get('/subcat-list',[AdminController::class, 'subcat_profile'])->name('subcat_profile');
Route::get('/subcat-edit/{scat_id}', [AdminController::class, 'subcat_edit'])->name('subcat_edit');
Route::post('subcat_update', [AdminController::class, 'subcat_update'])->name('subcat_update');
Route::delete('subcat-delete/{scat_id}', [AdminController::class, 'subcat_destroy'])->name('subcat_destroy');
Route::post('subcat_approve/{scat_id}', [AdminController::class, 'subcat_approve'])->name('subcat_approve');
Route::post('subcat_decline/{scat_id}', [AdminController::class, 'subcat_decline'])->name('subcat_decline');



	
      //media
      Route::get('/media',[MediaController::class, 'media'])->name('media');
      Route::get('/loadfile',[MediaController::class, 'loadfile'])->name('loadfile');
      Route::get('/loadfile_json',[MediaController::class, 'loadfile_json'])->name('loadfile_json');
      Route::post('/newloadfile_json',[MediaController::class, 'newloadfile_json'])->name('newloadfile_json');
      Route::post('/media_upload',[MediaController::class, 'media_upload'])->name('media_upload');
      Route::post('/media_delete',[MediaController::class, 'media_delete'])->name('media_delete');
      Route::post('/nxtloadfile',[MediaController::class, 'nxtloadfile'])->name('nxtloadfile');
      //endmedia
	Route::get('/',[AdminController::class, 'index'])->name('/');
	Route::post('/admin_loginCheck',[AdminController::class, 'admin_loginCheck'])->name('admin_loginCheck');
	Route::get('/dashboard',[AdminController::class, 'admin_dashboard'])->name('admin_dashboard');
	Route::match(['get','post'],'/users',[AdminController::class, 'users'])->name('users');
	//Catalog//
	Route::get('/category',[AdminController::class, 'category'])->name('category');
	Route::post('/add_category',[AdminController::class, 'add_category'])->name('add_category');
	Route::match(['get','post'],'/category-list',[AdminController::class, 'category_list'])->name('category_list');
	Route::post('/fetch_cat_data',[AdminController::class, 'fetch_cat_data'])->name('fetch_cat_data');
	Route::post('/edit_category',[AdminController::class, 'edit_category'])->name('edit_category');
	//SubCatalog//
	Route::get('/sub-category',[AdminController::class, 'sub_category'])->name('sub_category');
	Route::post('/add_subcategory',[AdminController::class, 'add_subcategory'])->name('add_subcategory');
	Route::match(['get','post'],'/subcategory-list',[AdminController::class, 'subcategory_list'])->name('subcategory_list');
	Route::post('/fetch_scat_data',[AdminController::class, 'fetch_scat_data'])->name('fetch_scat_data');
	Route::post('/edit_subcategory',[AdminController::class, 'edit_subcategory'])->name('edit_subcategory');
	//Brand//
	Route::get('/brand',[AdminController::class, 'brand'])->name('brand');
	Route::post('/add_brand',[AdminController::class, 'add_brand'])->name('add_brand');
	Route::match(['get','post'],'/brand-list',[AdminController::class, 'brand_list'])->name('brand_list');
	Route::post('/fetch_brand_data',[AdminController::class, 'fetch_brand_data'])->name('fetch_brand_data');
	Route::post('/edit_brand',[AdminController::class, 'edit_brand'])->name('edit_brand');
	//Attribute//
	Route::get('/attribute',[AdminController::class, 'attribute'])->name('attribute');
	Route::post('/add_attribute',[AdminController::class, 'add_attribute'])->name('add_attribute');
	Route::match(['get','post'],'/attribute-list',[AdminController::class, 'attribute_list'])->name('attribute_list');
	Route::post('/fetch_attribute_data',[AdminController::class, 'fetch_attribute_data'])->name('fetch_attribute_data');
	Route::post('/edit_attribute',[AdminController::class, 'edit_attribute'])->name('edit_attribute');
});

