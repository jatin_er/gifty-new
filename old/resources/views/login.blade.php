<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="assets/img/basic/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Vendor Login</title>
    <!-- CSS -->
    <link rel="stylesheet" href="{{('assets/css/app.css')}}">
      <link rel="stylesheet" href="{{ URL::asset('//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css')}}">
    <style>
        .loader {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: #F5F8FA;
            z-index: 9998;
            text-align: center;
        }

        .plane-container {
            position: absolute;
            top: 50%;
            left: 50%;
        }
    </style>
</head>
<body class="light">
<!-- Pre loader -->
 <div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div> 
<div id="app">
<main>
    <div id="primary" class="p-t-b-100 height-full ">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mx-md-auto">
                    <div class="text-center">
                        <img src="assets/img/dummy/u5.png" alt="">
                        <h3 class="mt-2">Welcome Back</h3>
                        <p class="p-t-b-20">Hey Soldier welcome back signin now there is lot of new stuff waiting
                            for you</p>
                    </div>
                    <form action="{{ route('login') }}" method="post" >
                         <!-- @csrf -->
                        <div class="form-group has-icon"><i class="icon-envelope-o"></i>
                            <input type="email" name="email" id="email" class="form-control form-control-lg"
                                   placeholder="Email Address" required="">
                        </div>
                        <div class="form-group has-icon"><i class="icon-user-secret"></i>
                            <input type="password"name="password" id="password" class="form-control form-control-lg"
                                   placeholder="Password" required="">
                        </div>
                        <input type="submit" class="btn btn-success btn-lg btn-block" id="login-form"name="submit" value="Log In">
                        <p class="forget-pass">Have you forgot your username or password ?</p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #primary -->
</main>
</div>
<!--/#app -->
<script src="assets/js/app.js"></script>
<script src="{{ URL::asset('//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js')}}"></script>
<script type="text/javascript">

 $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#login-form").click(function(e){
      e.preventDefault();
      // alert('h')
      // console.log('detected');

      $.post($(this).attr('action'),
        {
            email:$('#email').val(),
            password:$('#password').val()

        },
        function(response){
            console.log(response);
            if(response.success==false)
              {
                 Swal.fire({
                  title: 'Please Enter Valid Email Address.',
                  type: 'warning',
                  showCloseButton: true
                })
                  
              }
           else if(response.success==true){
    
                    Swal.fire({
                    title: 'Login successfully',
                    type: 'success',
                    showCloseButton: true
                })
                       location.href = 'index';
                  }
            else{
                Swal.fire({
                title: 'Please specify a valid email address',
                type: 'error',
                showCloseButton: true
                })
          
                  }

      });

  });


</script>

</body>
</html>