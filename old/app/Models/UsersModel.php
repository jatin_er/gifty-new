<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersModel extends Model
{
    // use HasFactory;
   
    protected $table = 'users';
	public $timestamps = true;
	protected $fillable = ['country_id','name','last_name','email','phone','company_name','i_am_a','password'];

}
