<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weights', function (Blueprint $table) {
            $table->increments('weight_id');
            $table->text('weight_name')->nullable();
            $table->text('weight_label_name')->nullable();
            $table->integer('weight_order')->default('0');
            $table->integer('weight_status')->default('0');
            $table->string('weight_date',10)->nullable();
            $table->string('weight_time',10)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weights');
    }
}
