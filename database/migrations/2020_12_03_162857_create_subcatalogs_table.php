<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcatalogs', function (Blueprint $table) {
            $table->increments('scat_id');
            $table->integer('cat_id')->default('0');
            $table->text('scat_name')->nullable();
            $table->text('scat_slug')->nullable();
            $table->text('seokey')->nullable();
            $table->text('meta_description')->nullable();
            $table->integer('scat_order')->default('0');
            $table->integer('scat_status')->default('0');
            $table->string('scat_date',10)->nullable();
            $table->string('scat_time',10)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcatalogs');
    }
}
